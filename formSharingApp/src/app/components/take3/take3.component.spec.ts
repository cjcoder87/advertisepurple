import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Take3Component } from './take3.component';

describe('Take3Component', () => {
  let component: Take3Component;
  let fixture: ComponentFixture<Take3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Take3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Take3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
