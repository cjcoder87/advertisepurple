import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Take2Component } from './take2.component';

describe('Take2Component', () => {
  let component: Take2Component;
  let fixture: ComponentFixture<Take2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Take2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Take2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
