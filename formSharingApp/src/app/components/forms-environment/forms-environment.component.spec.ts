import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsEnvironmentComponent } from './forms-environment.component';

describe('FormsEnvironmentComponent', () => {
  let component: FormsEnvironmentComponent;
  let fixture: ComponentFixture<FormsEnvironmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormsEnvironmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsEnvironmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
