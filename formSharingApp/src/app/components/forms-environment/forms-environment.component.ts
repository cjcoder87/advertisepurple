import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { HomeService } from 'src/app/services/home.service';

@Component({
  selector: 'app-forms-environment',
  templateUrl: './forms-environment.component.html',
  styleUrls: ['./forms-environment.component.css']
})
export class FormsEnvironmentComponent implements OnInit {

  constructor(private homeService:HomeService,private router:Router) { }
showLeft=true;
showRight=true;
leftIndex=0;
rightIndex=0;
  ngOnInit(): void {
    this.showRight = this.showRight= this.homeService.conversion.hideRight;
    this.showLeft = this.showRight= this.homeService.conversion.hideLeft;
    this.leftIndex = this.homeService.conversion.left;
    this.rightIndex = this.homeService.conversion.right;
  }

  navigateBack(): void {
    this.router.navigateByUrl('/take'+this.leftIndex.toString());
  }

  navigateForward(): void {
    this.router.navigateByUrl('/take'+this.rightIndex.toString());
  }
}
