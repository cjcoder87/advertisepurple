import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/services/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  items:any = [
    {id:1,name:"first"},
    {id:2,name:"second"},
    {id:3,name:"third"}
  ]
  constructor(private homeService:HomeService) {

   }

  ngOnInit(): void {
  }

  getPosition(i:number, length:number):any{
    let conversion={
      left:(i),
      right:(i+1),
      hideRight:false,
      hideLeft:false
    };
  // let left:number=i;
  // let right:number=i+1;

    //Last Page
    if(length-(i+1)==0){

      //Its at the end, the right arrow needs to hide
      conversion.hideRight=false;
      conversion.hideLeft=true;
     // conversion.left=(i)
    // conversion.right=(i+1)
    }else if(i=0){
//the left arrow needs to hide
conversion.hideRight=true;
conversion.hideLeft=false;
    }else{
      conversion.hideRight=true
conversion.hideLeft=true;
    }
    this.homeService.conversion= conversion;
  }

}
