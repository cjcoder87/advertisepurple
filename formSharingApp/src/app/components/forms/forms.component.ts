import { Component, OnInit } from '@angular/core';
import { FormShareService } from 'src/app/services/form-share.service';
import { Comment} from '../../interface/comment';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {
   comment:Comment =
    {
      comment:"",
      description:""
    };

  constructor(private formShareService:FormShareService) { }

  ngOnInit(): void {
   let recievedComment:Comment = this.formShareService.getComments();
   this.checkComments(recievedComment);
  }

  checkComments(recievedComment:Comment): void {
    if(recievedComment.comment!=""){
      this.comment.comment = recievedComment.comment;
      console.log("form pulls comments from Service");
    }
    if(recievedComment.description!=""){
      this.comment.description= recievedComment.description;
      console.log("form pulls description from Service");
    }
  }

  setComments(): void {
    let timeout:any;
    clearTimeout(timeout);
  timeout =  setTimeout(() => {
      this.formShareService.comment.comment=this.comment.comment;
     // this.formShareService.getComments();
      console.log("Service Comment Object set from form, someone typed in an input");
    }, 600);
  }

  setDescription(): void {
    let timeout:any;
    clearTimeout(timeout);
  timeout =  setTimeout(() => {
      this.formShareService.comment.description=this.comment.description;
     // this.formShareService.getComments();
      console.log("Service Comment Object set from form, someone typed in an input");
    }, 600);
  }

}
