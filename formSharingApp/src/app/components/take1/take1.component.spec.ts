import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Take1Component } from './take1.component';

describe('Take1Component', () => {
  let component: Take1Component;
  let fixture: ComponentFixture<Take1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Take1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Take1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
