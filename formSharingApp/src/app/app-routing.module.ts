import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { Take1Component } from './components/take1/take1.component';
import { Take2Component } from './components/take2/take2.component';
import { Take3Component } from './components/take3/take3.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'first', component: Take1Component },
  { path: 'second', component: Take2Component },
  { path: 'third', component: Take3Component },
 { path: '', component: HomeComponent, pathMatch:"full" },
 { path: '**', component: HomeComponent, pathMatch:"full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
