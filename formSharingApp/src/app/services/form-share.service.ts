import { Injectable, OnDestroy } from '@angular/core';
import { Comment} from '../interface/comment';

@Injectable({
  providedIn: 'root'
})
export class FormShareService{

  constructor() { }

  comment:Comment =
  {
    comment:"",
    description:""
  };

  getComments(): Comment {
    console.log(this.comment.comment + " comment value returning out of service");
    console.log(this.comment.description + " description value returning out of service");
    return this.comment;
  }

}
