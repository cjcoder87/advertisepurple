import { TestBed } from '@angular/core/testing';

import { FormShareService } from './form-share.service';

describe('FormShareService', () => {
  let service: FormShareService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormShareService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
